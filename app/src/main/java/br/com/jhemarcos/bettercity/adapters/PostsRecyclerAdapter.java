package br.com.jhemarcos.bettercity.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.jhemarcos.bettercity.R;
import br.com.jhemarcos.bettercity.models.Posts;

public class PostsRecyclerAdapter  extends RecyclerView.Adapter<PostsRecyclerAdapter.ViewHolder>{

    private List<Posts> itemList;
    private Context context;
    private RequestManager imageLoader;
    private RecyclerView recyclerView;

    public PostsRecyclerAdapter(Context context, List<Posts> itemList) {
        this.itemList = itemList;
        this.context = context;
        imageLoader = Glide.with(context);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_post, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Posts post = itemList.get(position);

        holder.userName.setText(post.getUser().getFirst_name() + " " + post.getUser().getLast_name());
        imageLoader.load(post.getUser().getAvatar()).into(holder.userAvatar);

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = formatter.parse(post.getCreatedAt());
            long timestamp = date.getTime();
            String timeFromNow = DateUtils.getRelativeTimeSpanString(timestamp).toString();
            holder.postEvent.setText(timeFromNow + " - " + post.getPlaceName());
        } catch (Exception e){
            holder.postEvent.setText(post.getCreatedAt() + " - " + post.getPlaceName());
        }

        holder.description.setText(post.getDescription());

        if(!post.getMediaPosts().isEmpty()){
            holder.postImage.setVisibility(View.VISIBLE);
            imageLoader.load(post.getMediaPosts().get(0).getUrl()).into(holder.postImage);
        } else {
            holder.postImage.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView userName;
        TextView postEvent;
        TextView postReactions;
        TextView description;
        ImageView userAvatar;
        ImageView postImage;
        View btnCurtir;
        View btnComentar;

        ViewHolder(View itemView) {
            super(itemView);
            userName = (TextView)itemView.findViewById(R.id.user_name);
            userAvatar = (ImageView)itemView.findViewById(R.id.user_avatar);
            postImage = (ImageView)itemView.findViewById(R.id.post_image);
            postEvent = (TextView)itemView.findViewById(R.id.post_event);
            postReactions = (TextView)itemView.findViewById(R.id.post_reactions);
            btnCurtir = itemView.findViewById(R.id.post_curtir);
            btnComentar = itemView.findViewById(R.id.post_comentar);
            description = (TextView) itemView.findViewById(R.id.description);
        }

    }
}
