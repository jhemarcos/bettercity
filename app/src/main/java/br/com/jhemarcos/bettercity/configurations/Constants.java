package br.com.jhemarcos.bettercity.configurations;

public class Constants {
    public static final int SPLASH_TIME_OUT = 3000;
    public static final String API_URL = "https://bettercity.jhemarcos.com.br/api";
    public static final String LOGIN_API_URL = API_URL + "/login";
    public static final String POSTS_API_URL = API_URL + "/posts";
}
