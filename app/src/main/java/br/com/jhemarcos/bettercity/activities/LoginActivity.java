package br.com.jhemarcos.bettercity.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import br.com.jhemarcos.bettercity.R;
import br.com.jhemarcos.bettercity.utils.AccountUtils;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Verificar se o usuário está ou não logado
        if (AccountUtils.hasActiveAccount(this)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void setWidgetReferences() {

    }

    @Override
    public int getLayoutContentView() {
        return R.layout.activity_login;
    }

    @Override
    public void bindEventHandlers() {

    }

}
