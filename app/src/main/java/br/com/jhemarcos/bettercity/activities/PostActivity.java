package br.com.jhemarcos.bettercity.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.jhemarcos.bettercity.R;
import br.com.jhemarcos.bettercity.models.User;
import br.com.jhemarcos.bettercity.utils.AccountUtils;

public class PostActivity extends BaseActivity {

    private ImageView userAvatar;
    private TextView userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setShowToolbarBackButton(true);

        User user = AccountUtils.getActiveAccount(this);

        Glide.with(this).load(user.getAvatar()).into(userAvatar);
        userName.setText(user.getFirst_name() + " " + user.getLast_name());
    }

    @Override
    public void setWidgetReferences() {
        userAvatar = (ImageView) findViewById(R.id.user_avatar);
        userName = (TextView) findViewById(R.id.user_name);
    }

    @Override
    public int getLayoutContentView() {
        return R.layout.activity_post;
    }

    @Override
    public void bindEventHandlers() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
