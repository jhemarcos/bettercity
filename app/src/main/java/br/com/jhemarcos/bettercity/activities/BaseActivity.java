package br.com.jhemarcos.bettercity.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import br.com.jhemarcos.bettercity.R;
import br.com.jhemarcos.bettercity.models.User;
import br.com.jhemarcos.bettercity.utils.AccountUtils;

public abstract class BaseActivity extends AppCompatActivity {

    protected Handler handler = new Handler();
    protected ProgressDialog progress;
    protected User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(AccountUtils.hasActiveAccount(this)){
            user = AccountUtils.getActiveAccount(this);
        }

        //Inicializando o progress
        progress = new ProgressDialog(this);

        //Setando o layout
        setContentView(getLayoutContentView());

        //Setando as referências de elementos no layout
        setWidgetReferences();

        //Configurando eventos
        bindEventHandlers();

    }

    /**
     * Configura as referências de elementos no layout
     */
    public abstract void setWidgetReferences();

    /**
     * Retorna o layout da activity
     * @return O id da activity
     */
    public abstract int getLayoutContentView();

    /**
     * Seta os eventos necessários a cada objeto
     */
    public abstract void bindEventHandlers();

    /**
     * Altera a cor do toolbar
     * @param colorCode String o código da cor Ex. #000000
     */
    public void setToolbarColor(String colorCode) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(colorCode)));
        }
    }

    /**
     * Altera o título do toolbar
     * @param title String O novo título
     */
    public void setToolbarTitle(String title) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    /**
     * Exibe o botão de voltar no toolbar
     * @param show boolean
     */
    public void setShowToolbarBackButton(boolean show) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(show);
        }
    }

    /**
     * Exibe um toask rodando na Thread UI
     * @param text String
     */
    public void showToast(final String text){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, text, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Exibe um progress não cancelavel com a menssagem passada
     * @param message String
     */
    public void showProgress(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setMessage(message);
                if(!progress.isShowing()){
                    progress.show();
                }
            }
        });
    }

    /**
     * Oculta o progress
     */
    public void hideProgress(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(progress.isShowing()){
                    progress.dismiss();
                }
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_forward, R.anim.slide_out_right);
    }
}
