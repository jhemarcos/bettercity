package br.com.jhemarcos.bettercity.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.List;

import br.com.jhemarcos.bettercity.R;
import br.com.jhemarcos.bettercity.adapters.PostsRecyclerAdapter;
import br.com.jhemarcos.bettercity.configurations.Constants;
import br.com.jhemarcos.bettercity.models.Posts;
import br.com.jhemarcos.bettercity.utils.AccountUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FeedActivity extends BaseActivity {

    private DrawerLayout nDrawerLayout;
    private ActionBarDrawerToggle nToggle;
    private LinearLayoutManager linearLayoutManager;

    private TextView emailDrawer;
    private TextView nomeDrawer;
    private ImageView avatarDrawer;
    private NavigationView navigationView;
    private RecyclerView postsRecyclerView;
    private ProgressBar progressBar;
    private FloatingActionButton fab;

    private List<Posts> posts;

    private String nextPageUrl = Constants.POSTS_API_URL;
    private boolean paginationLoad = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nToggle.syncState();

        setShowToolbarBackButton(true);
        loadFeed();
    }

    @Override
    public void setWidgetReferences() {
        postsRecyclerView = (RecyclerView) findViewById(R.id.posts_recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        nDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nToggle = new ActionBarDrawerToggle(this, nDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        navigationView = (NavigationView) nDrawerLayout.findViewById(R.id.navigationView);
        View headerLayout = navigationView.getHeaderView(0);

        emailDrawer = (TextView) headerLayout.findViewById(R.id.emailDrawer);
        nomeDrawer = (TextView) headerLayout.findViewById(R.id.nomeDrawer);
        avatarDrawer = (ImageView) headerLayout.findViewById(R.id.avatarDrawer);

        linearLayoutManager = new LinearLayoutManager(FeedActivity.this);

        configureNavDrawer();
    }

    @Override
    public int getLayoutContentView() {
        return R.layout.activity_feed;
    }

    @Override
    public void bindEventHandlers() {
        nDrawerLayout.addDrawerListener(nToggle);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_stories:

                        return true;
                    case R.id.nav_settings:

                        return true;
                    case R.id.nav_logout:
                        AccountUtils.setActiveAccount(FeedActivity.this, null);
                        Intent intent = new Intent(FeedActivity.this, SplashScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        return true;
                    default:
                        return true;
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FeedActivity.this, PostActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(nToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.feed_menu, menu);
        return true;
    }

    /**
     * Configura as views do nav drawer
     */
    private void configureNavDrawer(){
        emailDrawer.setText(user.getEmail());
        nomeDrawer.setText(user.getFirst_name() + " " + user.getLast_name());
        Glide.with(FeedActivity.this).load(user.getAvatar()).into(avatarDrawer);
    }

    /**
     * Carrega os posts do feed de acordo com os parâmetros
     */
    private void loadFeed(){
        showProgress("Carregando...");
        paginationLoad = true;
        progressBar.setVisibility(View.VISIBLE);

        OkHttpClient okHttpClient = new OkHttpClient();

        Request request = new Request.Builder()
                .url(nextPageUrl)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                paginationLoad = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

                e.printStackTrace();
                showToast("Não foi possível carregar os posts no momento: " + e.getMessage());
                hideProgress();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                paginationLoad = false;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
                try {
                    String responseBody = response.body().string();
                    JSONObject jsonObj = (JSONObject) new JSONTokener(responseBody).nextValue();
                    String arrayData = jsonObj.getString("data");

                    posts = new Gson().fromJson(arrayData, new TypeToken<List<Posts>>() {}.getType());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            postsRecyclerView.setLayoutManager(linearLayoutManager);
                            postsRecyclerView.setHasFixedSize(true);
                            PostsRecyclerAdapter relationshipCardAdapter = new PostsRecyclerAdapter(FeedActivity.this, posts);
                            postsRecyclerView.setAdapter(relationshipCardAdapter);
                            postsRecyclerView.addOnScrollListener(createInfiniteScrollListener());
                            hideProgress();
                        }
                    });

                    JSONObject jsonMeta = jsonObj.getJSONObject("meta");
                    if(jsonMeta.has("nextPage")){
                        nextPageUrl = jsonMeta.getString("nextPage");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    showToast("Ocorreu um erro ao transformar os posts: " + e.getMessage());
                }
            }
        });

    }

    /**
     * Habilitar o scroll infinito
     * @return um objeto listener do infinite scroll
     */
    private InfiniteScrollListener createInfiniteScrollListener() {
        return new InfiniteScrollListener(6, linearLayoutManager) {
            @Override public void onScrolledToEnd(final int firstVisibleItemPosition) {

                //Se já está na última página ou já está carregando uma paginação
                if(nextPageUrl == null || paginationLoad){
                    return;
                }

                paginationLoad = true;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });


                OkHttpClient okHttpClient = new OkHttpClient();

                Request request = new Request.Builder()
                        .url(nextPageUrl)
                        .build();

                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        paginationLoad = false;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        e.printStackTrace();
                        showToast("Não foi possível carregar os posts no momento: " + e.getMessage());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        paginationLoad = false;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        try {
                            String responseBody = response.body().string();
                            JSONObject jsonObj = (JSONObject) new JSONTokener(responseBody).nextValue();
                            String arrayData = jsonObj.getString("data");

                            List<Posts> newPosts = new Gson().fromJson(arrayData, new TypeToken<List<Posts>>() {}.getType());
                            posts.addAll(newPosts);

                            JSONObject jsonMeta = jsonObj.getJSONObject("meta");
                            if(jsonMeta.has("nextPage")){
                                nextPageUrl = jsonMeta.getString("nextPage");
                            }

                            if(jsonMeta.getInt("currentPage") == jsonMeta.getInt("lastPage")){
                                nextPageUrl = null;
                            }

                            Log.d("logger", "call onResponse");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    postsRecyclerView.setLayoutFrozen(true);
                                    refreshView(postsRecyclerView, new PostsRecyclerAdapter(FeedActivity.this, posts), firstVisibleItemPosition);
                                    postsRecyclerView.setLayoutFrozen(false);
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                            showToast("Ocorreu um erro ao transformar os posts: " + e.getMessage());
                        }
                    }
                });

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                if (dy >0) {
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                }
                else if (dy <0) {
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }

        };
    }
}
