package br.com.jhemarcos.bettercity.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;

import br.com.jhemarcos.bettercity.R;
import br.com.jhemarcos.bettercity.configurations.Constants;
import br.com.jhemarcos.bettercity.models.User;
import br.com.jhemarcos.bettercity.utils.AccountUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SplashScreenActivity extends BaseActivity {

    private GoogleSignInOptions gso;

    private GoogleApiClient mGoogleApiClient;

    private int RC_SIGN_IN = 100;

    private ImageView imgLogo;
    private TextView hostname;
    private View loginBtn;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                ScaleAnimation scaleAnimation = new ScaleAnimation((float) 1.0, (float) 0.75, (float) 1.0, (float) 0.75, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, 0, -400);

                AnimationSet animationSet = new AnimationSet(false);
                animationSet.addAnimation(scaleAnimation);
                animationSet.addAnimation(translateAnimation);
                animationSet.setFillAfter(true);
                animationSet.setDuration(2000);

                imgLogo.startAnimation(animationSet);
                hostname.setVisibility(View.GONE);

                if(AccountUtils.hasActiveAccount(SplashScreenActivity.this)) {
                    loginBtn.setVisibility(View.GONE);
                    showProgress("Carregando...");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideProgress();
                            Intent intent = new Intent(SplashScreenActivity.this, FeedActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }, 3000);

                } else {
                    loginBtn.setVisibility(View.VISIBLE);
                    AlphaAnimation alphaAnimation = new AlphaAnimation((float) 0, (float) 1);
                    alphaAnimation.setFillAfter(true);
                    alphaAnimation.setDuration(3000);
                    loginBtn.startAnimation(alphaAnimation);
                }
            }
        }, Constants.SPLASH_TIME_OUT);

        configureGoogleLogin();
    }

    /**
     * Realiza configurações necessárias ao login com o google
     */
    private void configureGoogleLogin() {
        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        showToast("Google API Connection Failed.");
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void setWidgetReferences() {
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        hostname = (TextView) findViewById(R.id.hostname);
        loginBtn = findViewById(R.id.loginBtn);
    }

    @Override
    public int getLayoutContentView() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public void bindEventHandlers() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

    //Inicia o fluxo de login
    private void signIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            showProgress("Carregando...");

            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.d("logger", "nome: " + acct.getDisplayName());
            Log.d("logger", "email: " + acct.getEmail());
            Log.d("logger", "avatar: " + acct.getPhotoUrl().toString());


            OkHttpClient okHttpClient = new OkHttpClient();

            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("email", acct.getEmail())
                    .addFormDataPart("given_name", acct.getGivenName())
                    .addFormDataPart("family_name", acct.getFamilyName())
                    .addFormDataPart("avatar", acct.getPhotoUrl().toString())
                    .build();

            Request request = new Request.Builder()
                    .url(Constants.LOGIN_API_URL)
                    .post(formBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    hideProgress();
                    showToast("Erro ao realizar o login: " + e.getMessage());
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    try {
                        String responseBody = response.body().string();

                        JSONObject jsonObj = (JSONObject) new JSONTokener(responseBody).nextValue();
                        User newUser = new Gson().fromJson(jsonObj.getString("data"), User.class);

                        AccountUtils.setActiveAccount(SplashScreenActivity.this, newUser);
                        hideProgress();

                        Intent intent = new Intent(SplashScreenActivity.this, FeedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        hideProgress();
                        showToast("Erro ao realizar o login: " + ex.getMessage());
                    }
                }
            });

        } else {
            showToast("Erro ao realizar o login. Não foi possível acessar sua conta do Google.");
        }
    }
}
