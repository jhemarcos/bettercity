package br.com.jhemarcos.bettercity.activities;

import android.os.Bundle;

import br.com.jhemarcos.bettercity.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setWidgetReferences() {

    }

    @Override
    public int getLayoutContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void bindEventHandlers() {

    }
}
