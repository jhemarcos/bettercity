package br.com.jhemarcos.bettercity.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import br.com.jhemarcos.bettercity.models.User;

public class AccountUtils {

    private static final String PREF_ACTIVE_ACCOUNT = "chosen_account";

    private static User activeAccount;

    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Verifica se existe uma conta logada
     * @param context O contexto que está chamando
     * @return Boleano se existe ou não uma conta logada
     */
    public static boolean hasActiveAccount(final Context context) {
        return getActiveAccount(context) != null;
    }

    /**
     * Retorna a conta que está logada na aplicação
     * @param context O contexto que está chamando
     * @return O usuário que está logado ou null
     */
    public static User getActiveAccount(final Context context) {
        if (activeAccount == null) {
            SharedPreferences sp = getSharedPreferences(context);
            String json = sp.getString(PREF_ACTIVE_ACCOUNT, null);
            try {
                if (json != null) {
                    Gson gson = new Gson();
                    activeAccount = gson.fromJson(json, User.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return activeAccount;
    }

    /**
     * Altera o usuário que está logado no aplicação
     * @param context O contexto que está chamando
     * @param user O novo usuário que será logado
     */
    @SuppressLint("CommitPrefEdits")
    public static void setActiveAccount(final Context context, final User user) {
        SharedPreferences sp = getSharedPreferences(context);
        activeAccount = user;
        if (user != null) {
            Gson gson = new Gson();
            String jSon = gson.toJson(user);
            sp.edit().putString(PREF_ACTIVE_ACCOUNT, jSon).commit();
        } else {
            sp.edit().remove(PREF_ACTIVE_ACCOUNT).commit();
        }
    }

}
