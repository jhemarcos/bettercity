![logo-site.png](https://bitbucket.org/repo/LooGbg6/images/3447007605-logo-site.png) 
# BETTERCITY

Repositório da aplicação móvel para android do BetterCity.

### Acesso ao repositório ###

Este repositório é privado por conter dados de acesso sensíveis e estará aberto apenas durante o período de avaliação.

### Acesso via site ###
O serviço BetterCity também está disponível na web no endereço [https://bettercity.jhemarcos.com.br](https://bettercity.jhemarcos.com.br).